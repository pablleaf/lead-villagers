# Lead Villagers

*Lead* your villagers to work.

## Information

Check out this mod on [CurseForge][].

## Building from source

```bash
git clone https://gitlab.com/sargunv-mc-mods/lead-villagers.git
cd lead-villagers
./gradlew build
# On Windows, use "gradlew.bat" instead of "gradlew"
```

[CurseForge]: https://minecraft.curseforge.com/projects/lead-villagers
