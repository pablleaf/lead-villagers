package me.sargunvohra.mcmods.leadvillagers;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.Npc;
import net.minecraft.entity.passive.AbstractTraderEntity;
import net.minecraft.entity.passive.PassiveEntity;
import net.minecraft.village.Trader;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(AbstractTraderEntity.class)
abstract class AbstractTraderEntityMixin extends PassiveEntity implements Npc, Trader {
    private AbstractTraderEntityMixin(EntityType<? extends PassiveEntity> type, World world) {
        super(type, world);
    }

    @Inject(method = "canBeLeashedBy", at = @At("RETURN"), cancellable = true)
    private void onCanBeLeashedBy(CallbackInfoReturnable<Boolean> cir) {
        cir.setReturnValue(cir.getReturnValue() || !this.isLeashed());
    }
}
